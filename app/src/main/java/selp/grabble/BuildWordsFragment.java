package selp.grabble;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Fragment containing page for spelling words.
 *
 * //http://stackoverflow.com/questions/17326524/change-many-imageview-sources-using-for-loop
 */
public class BuildWordsFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();

    ImageView blank1, blank2, blank3, blank4, blank5, blank6, blank7;
    ImageView[] blankTiles;
    TextView totalPoints;

    CardView doubleLetter;
    CardView tripleLetter;
    CardView doubleWord;
    CardView tripleWord;
    private MainActivity activity;

    public BuildWordsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_build_words, container, false);

        activity = (MainActivity) this.getActivity();

        //set current points and user name
        totalPoints = (TextView) v.findViewById(R.id.totalPoints);
        updateTotalPoints();

        TextView userName = (TextView) v.findViewById(R.id.username);
        userName.setText(activity.userName);

        //set letterCount
        setLetterCount(v);

        //get current word value
        TextView tVwordValue = (TextView) v.findViewById(R.id.wordPoints);
        tVwordValue.setText(String.valueOf(activity.calculateWordPoints(activity.currentWord)));

        //initialise views
        blank1 = (ImageView) v.findViewById(R.id.blank_button1);
        blank2 = (ImageView) v.findViewById(R.id.blank_button2);
        blank3 = (ImageView) v.findViewById(R.id.blank_button3);
        blank4 = (ImageView) v.findViewById(R.id.blank_button4);
        blank5 = (ImageView) v.findViewById(R.id.blank_button5);
        blank6 = (ImageView) v.findViewById(R.id.blank_button6);
        blank7 = (ImageView) v.findViewById(R.id.blank_button7);

        blankTiles = new ImageView[]{blank1, blank2, blank3, blank4, blank5, blank6, blank7};

        //set current letter in blank tiles and current word count
        setCurrentWordToTiles();

        //set count of super powers
        setCountOfSpecial(v);

        return v;
    }

    public void updateTotalPoints() {
        totalPoints.setText(String.valueOf(activity.totalPoints));
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void deleteWord(Boolean pointsBack) {

        for (int i = 0; i < activity.currentWord.length(); i++) {
            //replace letter tiles with blank tiles
            int id = getResources().getIdentifier("blank", "drawable", activity.getPackageName());

            if (blankTiles[i] != null) {
                blankTiles[i].setImageResource(id);
            }
            //if word is deleted by delete button, restore the correct letter count
            if (pointsBack) {
                //add lettercount
                String currentLetter = Character.toString(activity.currentWord.charAt(i)).toLowerCase();
                int index = activity.alphabet.indexOf(currentLetter);
                activity.letterCount[index]++;
            }
        }
        activity.currentWord = "";
        updateWordPoints();
        updateSpecialCount();

        if (pointsBack) {
            setLetterCount(getView());
        }
    }

    /**
     * replace blank tile with correct letter
     * @param letter
     * @param index
     */
    public void addLetter(String letter, int index) {
        if (index < blankTiles.length) {
            ImageView current = blankTiles[index];

            int id = getResources().getIdentifier(letter, "drawable", activity.getPackageName());
            current.setImageResource(id);
        }
    }

    /**
     * Updates count of a certain letter
     * @param letter
     * @param index
     */
    public void updateSingleLetterCount(String letter, int index) {
        int id = getResources().getIdentifier(letter, "id", activity.getPackageName());
        View v = getView();
        TextView letterTile = (TextView) v.findViewById(id);
        if (letterTile != null) {
            letterTile.setText(String.valueOf(activity.letterCount[index]));
        } else {
            Log.d(TAG, "letterTile is null");
        }
    }

    public void updateWordPoints() {
        Log.d(TAG, "updateWordPoints");
        View v = getView();
        if (v != null) {
            TextView tVwordValue = (TextView) v.findViewById(R.id.wordPoints);
            tVwordValue.setText(String.valueOf(activity.calculateWordPoints(activity.currentWord)));
        } else {
            Log.d(TAG, "view is null");
        }
    }

    public void updateSpecialCount() {
        Log.d(TAG, "updateSpecialCount");
        View v = getView();
        setCountOfSpecial(v);
    }

    public void setLetterCount(View v) {

        for (int i = 0; i < activity.letterCount.length && i < activity.alphabet.size(); i++) {
            //set Count for each letter in keyboard
            String current = activity.alphabet.get(i);
            int id = getResources().getIdentifier(current, "id", activity.getPackageName());
            TextView letter = (TextView) v.findViewById(id);
            letter.setText(String.valueOf(activity.letterCount[i]));

        }
    }

    public void setCurrentWordToTiles() {
        for (int i = 0; i < activity.currentWord.length(); i++) {

            String currentLetter = Character.toString(activity.currentWord.charAt(i)).toLowerCase();
            int id = getResources().getIdentifier(currentLetter, "drawable", activity.getPackageName());

            if (blankTiles[i] != null) {
                blankTiles[i].setImageResource(id);
            } else {
                Log.d(TAG, "ImageView is null");
            }
        }
    }

    public void setCountOfSpecial(View v) {
        for (int i = 0; i < activity.specialCount.length && i < activity.specials.length; i++) {
            String current = activity.specials[i];
            int id = getResources().getIdentifier(current, "id", activity.getPackageName());
            TextView specialCount = (TextView) v.findViewById(id);
            specialCount.setText(String.valueOf(activity.specialCount[i]));
        }
    }

    /**
     * Change the background of special power star
     * @param name
     * @param darker
     */
    public void changeBackground(String name, boolean darker){
        Log.d(TAG, "changeBackground");

        View v = getView();

        doubleLetter = (CardView) v.findViewById(R.id.double_letter_star_view);
        tripleLetter = (CardView) v.findViewById(R.id.triple_letter_star_view);
        doubleWord = (CardView) v.findViewById(R.id.double_word_star_view);
        tripleWord = (CardView) v.findViewById(R.id.triple_word_star_view);

        //if view should be cleared
        if(!darker){
            Log.d(TAG, "clear background color");
            doubleLetter.setCardBackgroundColor(Color.TRANSPARENT);
            tripleLetter.setCardBackgroundColor(Color.TRANSPARENT);
            doubleWord.setCardBackgroundColor(Color.TRANSPARENT);
            tripleWord.setCardBackgroundColor(Color.TRANSPARENT);

        } else {
            Log.d(TAG, "change background color");

            int id = getResources().getIdentifier("colorSecondaryLight", "color", activity.getPackageName());

            if (name.equals(activity.specials[0])) {
                Log.d(TAG, "change Color to darker");
                doubleLetter.setCardBackgroundColor(id);

            } else if (name.equals(activity.specials[1])) {
                Log.d(TAG, "change Color to darker");
                tripleLetter.setCardBackgroundColor(id);

            } else if (name.equals(activity.specials[2])) {
                Log.d(TAG, "change Color to darker");
                doubleWord.setCardBackgroundColor(id);

            } else if (name.equals(activity.specials[3])) {
                Log.d(TAG, "change Color to darker");
                tripleWord.setCardBackgroundColor(id);

            } else {
                Log.d(TAG, "no color");
                return;
            }
        }
    }
}
