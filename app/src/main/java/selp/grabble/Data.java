package selp.grabble;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by nicole on 06.11.2016.
 */

public class Data {
    private static final String TAG = "Data";

    public static ArrayList<WordInfo> getDictionaryData() {

        String[] words = {
                "word1",
                "word2",
                "word3",
                "word4",
                "word5",
                "word6",
                "word7",
                "word8",
                "word9",
                "word10",
                "word11",
                "word12",
                "word13",
                "word14",
                "word15",
                "word16",
                "word17",
                "word18",
                "word19",
                "word20",
                "word21"
        };

        int[] points = {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
        };

        if (points.length == words.length) {
            Log.d(TAG, "points.length == words.length");
            ArrayList<WordInfo> data = new ArrayList<>();

            for (int i = 0; i < words.length; i++) {
                WordInfo info = new WordInfo(words[i], points[i], new Date());
                data.add(info);
            }


            return data;
        } else {
            Log.d(TAG, "points.length != words.length");
            return new ArrayList<>();
        }

    }

    public static ArrayList<WordInfo> getHighScoreData() {
        String[] words = {
                "word1",
                "word2",
                "word3",
                "word4",
                "word5",
                "word6",
                "word7",
                "word8",
                "word9",
                "word10",
                "word11",
                "word12",
                "word13",
                "word14",
                "word15",
                "word16",
                "word17",
                "word18",
                "word19",
                "word20",
                "word21"
        };
        int[] points = {
                21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
        };

        if (points.length == words.length) {
            Log.d(TAG, "points.length == words.length");
            ArrayList<WordInfo> data = new ArrayList<>();

            for (int i = 0; i < words.length; i++) {
                WordInfo info = new WordInfo(words[i], points[i], new Date());
                data.add(info);
            }


            return data;
        } else {
            Log.d(TAG, "points.length != words.length");
            return new ArrayList<>();
        }
    }

    public static ArrayList<HelpFiller> getHelpData() {

        String[] imageNames = {
                "collect_build",
                "progress_explain",
                "specials_collected",
                "navigation_letter",
                "keyboard",
                "check_delete",
                "highscore",
                "dictionary_explain"

        };

        String[] description= {
                "Collect letters by walking up to the location of a mysterious Marker and find out what letter was at that location. " +
                        "Start building words by clicking on the Build Now Button or choosing the option from the menu.",
                "Earn special power stars by walking a certain distance to fill the distance bar. " +
                        "View the total points you have earned by building words and the letters you have ever collected.",
                "Double or triple the highest letter value with the blue or yellow star. You can even double or triple the" +
                        " word value with the red or green star. Just choose the star, when building a word.",
                "Open the menu by sliding over the screen from right to left. See which letter is the " +
                        "nearest and open the map to grab it with selecting 'Grab Letter'.",
                "Build words by tapping on the letter tile in the correct order.  You may only use the letters, " +
                        "you have already collected.",
                "Check and submit the word by tapping 'Check'. Hit 'Delete' to unselect the chosen letters and special power stars.",
                "View your 20 best Words in the high scores.",
                "View all words you have ever built in the dictionary."

        };

        if (imageNames.length == description.length) {
            ArrayList<HelpFiller> data = new ArrayList<>();

            for (int i = 0; i < description.length; i++) {
                HelpFiller info = new HelpFiller(imageNames[i], description[i]);
                data.add(info);
            }

            return data;
        } else {
            Log.d(TAG, "points.length != words.length");
            return new ArrayList<>();
        }

    }


}
