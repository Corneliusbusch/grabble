package selp.grabble;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public class DictionaryActivity extends AppCompatActivity {
    private static final String TAG = "DictionaryActivity";

    RecyclerView recyclerView;

    DictionaryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Dictionary Activity OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        Log.d(TAG, "Dictionary Activity created");

        recyclerView = (RecyclerView) findViewById(R.id.dictionary_recycler_view);
        adapter = new DictionaryAdapter(this, Data.getDictionaryData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



    }
}
