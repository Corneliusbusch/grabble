package selp.grabble;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nicole on 06.11.2016.
 * Taken from: https://www.youtube.com/watch?v=o1ECbYo8eps
 */
public class DictionaryAdapter extends RecyclerView.Adapter<DictionaryAdapter.ViewHolder> {
    private static final String TAG = "DictionaryAdapter";

    Context context;
    ArrayList<WordInfo> data;
    LayoutInflater inflater;

    public DictionaryAdapter(Context context, ArrayList<WordInfo> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public DictionaryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //create view for each row
        View view = inflater.inflate(R.layout.dictionary_item, parent, false);
        DictionaryAdapter.ViewHolder holder = new DictionaryAdapter.ViewHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(DictionaryAdapter.ViewHolder holder, int position) {
        // insert data in each row
        Log.d(TAG, "before set Text at textViewWord");
        holder.textViewWord.setText(data.get(position).word);
        Log.d(TAG, "after set Text at textViewWord");
        Log.d(TAG, "before set Text at textViewPoints");
        String text = String.valueOf(data.get(position).maxPoints);
        holder.textViewPoints.setText(text);
        Log.d(TAG, "after set Text at textViewPoints");
    }


    @Override
    public int getItemCount() {
        //get number of list items
        Log.d(TAG, "data.size(): " + String.valueOf(data.size()));
        return data.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewWord;
        TextView textViewPoints;


        public ViewHolder(View itemView) {
            super(itemView);

            //view of single row
            textViewWord = (TextView) itemView.findViewById(R.id.item_word);
            textViewPoints = (TextView) itemView.findViewById(R.id.item_points);
        }

    }

}

