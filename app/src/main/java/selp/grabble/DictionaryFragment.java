package selp.grabble;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Fragment showing the words the user has built. It uses an Adapter to bind data to the view.
 *
 * //http://stackoverflow.com/questions/2012551/find-objects-in-a-list-where-some-attributes-have-equal-values
 *
 */
public class DictionaryFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();

    private RecyclerView recyclerView;
    private DictionaryAdapter adapter;
    private MainActivity activity;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dictionary, container, false);

        activity = (MainActivity) this.getActivity();

        TextView wordsBuilt = (TextView) v.findViewById(R.id.words_built);
        if (wordsBuilt != null) {
            Log.d(TAG, "wordsBuilt is null");
            wordsBuilt.setText(String.valueOf("You have built " + activity.userWords.size() + " / " + activity.dictionary.size()));
        }

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        recyclerView = (RecyclerView) getView().findViewById(R.id.dictionary_recycler_view);

        ArrayList<WordInfo> data = new ArrayList<>();

        //Bind data to recycler view
        if (activity.userWords.size() == 0) {
            data.add(new WordInfo("You haven't built any words.", 0, new Date()));
        } else {
            //eliminate duplicate words
            Log.d(TAG, "Size of userWords before remove duplicates: " + activity.userWords.size());
            data = (ArrayList<WordInfo>) activity.userWords.clone();
            data = removeDuplicates(data);
            Log.d(TAG, "Size of userWords after remove duplicates: " + activity.userWords.size());
        }

        adapter = new DictionaryAdapter(getActivity(), data);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


    }

    private ArrayList<WordInfo> removeDuplicates(ArrayList<WordInfo> listWithDuplicates) {
    /* Set of all attributes seen so far */
        Set<String> attributes = new HashSet<String>();
    /* All confirmed duplicates go in here */
        List duplicates = new ArrayList<WordInfo>();

        for (WordInfo x : listWithDuplicates) {
            if (attributes.contains(x.word)) {
                duplicates.add(x);
            }
            attributes.add(x.word);
        }

        listWithDuplicates.removeAll(duplicates);
    /* Clean list without any dups */
        return new ArrayList<WordInfo>(listWithDuplicates);
    }
}
