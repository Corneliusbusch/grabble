package selp.grabble;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nicole on 25.01.17.
 *
 //https://guides.codepath.com/android/Working-with-the-ImageView
 */

public class HelpAdapter extends RecyclerView.Adapter<selp.grabble.HelpAdapter.ViewHolder> {
    private static final String TAG = "HelpAdapter";
    Context context;
    ArrayList<HelpFiller> data;
    LayoutInflater inflater;

    public HelpAdapter(Context context, ArrayList<HelpFiller> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    public static Bitmap scaleToFitWidth(Bitmap b, int width) {
        float factor = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
    }

    @Override
    public selp.grabble.HelpAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //create view for each row
        View view = inflater.inflate(R.layout.help_item, parent, false);
        selp.grabble.HelpAdapter.ViewHolder holder = new selp.grabble.HelpAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(selp.grabble.HelpAdapter.ViewHolder holder, int position) {
        // insert data in each row
        holder.description.setText(data.get(position).description);


        int id = context.getResources().getIdentifier(data.get(position).imageName, "drawable", context.getPackageName());

        // Load a bitmap from the drawable folder
        Log.d(TAG, "decode resource");
        Bitmap bMap = BitmapFactory.decodeResource(context.getResources(), id);

        Log.d(TAG, "Get screen width");
        int screenWidth = getDisplayWidth(context);
        Bitmap bMapScaled = scaleToFitWidth(bMap, screenWidth);

        // Loads the resized Bitmap into an ImageView
        holder.image.setImageBitmap(bMapScaled);
    }

    @Override
    public int getItemCount() {
        //get number of list items
        Log.d(TAG, "data.size(): " + String.valueOf(data.size()));
        return data.size();
    }

    public static int getDisplayWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView description;
        ImageView image;


        public ViewHolder(View itemView) {
            super(itemView);

            //view of single row
            description = (TextView) itemView.findViewById(R.id.description);
            image = (ImageView) itemView.findViewById(R.id.help_image);

        }

    }

}


