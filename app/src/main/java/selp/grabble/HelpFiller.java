package selp.grabble;

/**
 * Created by nicole on 25.01.17.
 *
 * Class that represents the content of one card in the help page.
 */

public class HelpFiller {
    public String imageName;
    public String description;

    public HelpFiller(String imageName, String description){
        this.imageName = imageName;
        this.description = description;
    }

}
