package selp.grabble;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;

/**
 * Fragment containing the Help page.
 * //https:guides.codepath.com/android/Working-with-the-ImageView
 */
public class HelpFragment extends Fragment {

    private RecyclerView recyclerView;
    private HelpAdapter adapter;

    public HelpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_help, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();

        //Bind data to view
        recyclerView = (RecyclerView) getView().findViewById(R.id.help_recycler_view);

        adapter = new HelpAdapter(getActivity(), Data.getHelpData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
