package selp.grabble;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Adapter sets data into layout components.
 *
 * Created by nicole on 06.11.2016.
 * Taken from: https://www.youtube.com/watch?v=o1ECbYo8eps
 */
public class HighScoreAdapter extends RecyclerView.Adapter<HighScoreAdapter.ViewHolder> {
    private static final String TAG = "HighScoreAdapter";

    Context context;
    ArrayList<WordInfo> data;
    LayoutInflater inflater;

    public HighScoreAdapter(Context context, ArrayList<WordInfo> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public HighScoreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //create view for each row
        View view = inflater.inflate(R.layout.high_score_item, parent, false);
        HighScoreAdapter.ViewHolder holder = new HighScoreAdapter.ViewHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(HighScoreAdapter.ViewHolder holder, int position) {
        // insert data in each row
        holder.textViewWord.setText(data.get(position).word);

        String text = String.valueOf(data.get(position).maxPoints);
        holder.textViewPoints.setText(text);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        String date = sdf.format(data.get(position).firstDate);
        holder.textViewDate.setText(date);

        holder.textViewRank.setText(String.valueOf(position + 1));
    }


    @Override
    public int getItemCount() {
        //get number of list items
        Log.d(TAG, "data.size(): " + String.valueOf(data.size()));
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewWord;
        TextView textViewPoints;
        TextView textViewDate;
        TextView textViewRank;


        public ViewHolder(View itemView) {
            super(itemView);

            //view of single row
            textViewWord = (TextView) itemView.findViewById(R.id.item_word);
            textViewPoints = (TextView) itemView.findViewById(R.id.item_points);
            textViewDate = (TextView) itemView.findViewById(R.id.item_date);
            textViewRank = (TextView) itemView.findViewById(R.id.item_rank);
        }
    }
}

