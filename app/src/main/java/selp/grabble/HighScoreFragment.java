package selp.grabble;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Fragment that contains the high score.
 * //http://stackoverflow.com/questions/2012551/find-objects-in-a-list-where-some-attributes-have-equal-values
 */
public class HighScoreFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();

    RecyclerView recyclerView;
    HighScoreAdapter adapter;
    MainActivity activity;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) this.getActivity();
        return inflater.inflate(R.layout.fragment_high_score, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        recyclerView = (RecyclerView) getView().findViewById(R.id.high_score_recycler_view);

        //Get sorted data
        ArrayList<WordInfo> data = sortUserWords(removeDuplicates(activity.userWords));

        if (data.size() > 20) {
            data = new ArrayList<WordInfo>(data.subList(0, 20));
        } else if (data.size() == 0) {
            WordInfo wordInfo = new WordInfo("You have no high scores.", 0, new Date());
            data.add(wordInfo);
        }

        adapter = new HighScoreAdapter(getActivity(), data);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    /**
     * Sorts the arraylist by word points.
     *
     * @param words
     * @return sorted arraylist of WordInfo
     */
    private ArrayList<WordInfo> sortUserWords(ArrayList<WordInfo> words) {
        if (words.size() > 0) {
            Collections.sort(words, new Comparator<WordInfo>() {
                @Override
                public int compare(final WordInfo object1, final WordInfo object2) {
                    return object2.maxPoints - object1.maxPoints;
                }
            });
        }
        return words;
    }

    //removes words from userNames that have the same word and the same amount of points
    private ArrayList<WordInfo> removeDuplicates(ArrayList<WordInfo> listWithDuplicates) {

        if (listWithDuplicates.size() == 0) {
            return new ArrayList<>();
        }

        Log.d(TAG, "Remove duplicates");
        //Set of all attributes seen so far
        Set<Tuple> attributes = new HashSet<Tuple>();
        //All confirmed duplicates go in here
        List duplicates = new ArrayList<WordInfo>();

        for (WordInfo x : listWithDuplicates) {
            Tuple current = new Tuple(x.word, x.maxPoints);
            Log.d(TAG, "current: " + current.word + " " + current.points);

            for (Tuple tuple : attributes) {
                if (x.word.equals(tuple.word) && x.maxPoints == tuple.points) {
                    Log.d(TAG, "contains: " + current.toString());
                    duplicates.add(x);
                }
            }
            attributes.add(current);
        }

        listWithDuplicates.removeAll(duplicates);
        //Clean list without any duplicates
        return new ArrayList<>(listWithDuplicates);
    }

    private class Tuple extends Object {
        public final String word;
        public final int points;

        public Tuple(String x, int y) {
            this.word = x;
            this.points = y;
        }
    }
}
