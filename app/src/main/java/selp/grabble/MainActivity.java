package selp.grabble;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;


//https://www.youtube.com/watch?v=H4R-hz56oBA
//https://guides.codepath.com/android/Retrieving-Location-with-LocationServices-API
//http://stackoverflow.com/questions/5485807/android-pop-up-message
//https://github.com/google/gson
//http://stackoverflow.com/questions/14981233/android-arraylist-of-custom-objects-save-to-sharedpreferences-serializable
//http://stackoverflow.com/questions/24291721/reading-a-text-file-line-by-line-in-android
//http://stackoverflow.com/questions/16319237/cant-put-double-sharedpreferences
//http://stackoverflow.com/questions/7175880/how-can-i-store-an-integer-array-in-sharedpreferences
//http://stackoverflow.com/questions/8432581/how-to-sort-a-listobject-alphabetically-using-object-name-field
//http://stackoverflow.com/questions/25719620/how-to-solve-java-lang-outofmemoryerror-trouble-in-android
//http://stackoverflow.com/questions/18310126/get-the-distance-between-two-locations-in-android
//http://stackoverflow.com/questions/9664587/using-gps-get-the-distance-a-person-has-walked


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static final String SHARED_PREFS = "SharedPrefs";
    public static final int MY_ACCESS_FINE_LOCATION = 1;
    //radius for catching
    public static final int NORMAL_RADIUS = 10;
    public final int[] letterValue = {3, 20, 13, 10, 1, 15, 18, 9, 5, 25, 22, 11, 14, 6, 4, 19, 24, 8, 7, 2, 12, 21, 17, 23, 16, 26};
    public final List<String> alphabet = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
            "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    public final String[] specials = {"double_letter_star_count", "triple_letter_star_count", "double_word_star_count", "triple_word_star_count"};
    private final String TAG = this.getClass().getSimpleName();
    private final long UPDATE_INTERVAL = 12 * 1000;  /* 12 secs */
    private final long FASTEST_INTERVAL = 5 * 1000; /* 5 sec */

    //List of placemarks
    public ArrayList<PlaceMark> markers = new ArrayList<>();

    //global variables for gameflow
    public int totalPoints;
    public double distance;
    public String userName;
    public int[] letterCount;
    public String currentWord;
    public int lettersEverCollected;
    public int[] specialCount;
    public HashSet<String> dictionary;
    public ArrayList<WordInfo> userWords;
    public HashSet<String> visitedPlaceMarks;
    public int rewardUnlocked;
    public boolean doubleLetterClicked;
    public boolean tripleLetterClicked;
    public boolean doubleWordClicked;
    public boolean tripleWordClicked;
    private GoogleApiClient googleApiClient;
    public PlaceMark nearestLetter;
    public double nearestDistance;
    public Location lastLocation;
    public boolean collectionAllowed = false;
    private boolean dictionaryReady = false;

    //reference to fragments
    private MainFragment mainFragment;
    private BuildWordsFragment buildWordsFragment;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create google api client
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        //restore current game scores and adapt view
        restoreFromSharedPreferences();

        //load default fragment
        if (savedInstanceState == null) {
            mainFragment = new MainFragment();
            mainFragment.markerReady = false;
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    mainFragment,
                    mainFragment.getTag()
            ).commit();
        }

        //set up navigation drawer
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Set header
        if (userName != "") {
            setNavHeader();
        }

        //set up dictionary
        try {
            readDictionaryToHashSet();
        } catch (IOException e) {
            Log.e(TAG, "can not read dictionary to Hash set");
            e.printStackTrace();
        }

        //get current map markers
        try {
            getMap();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        //only ask for username at first start
        if (userName.equals("")) {
            showUserNameDialog();
        }
    }

    /**
     * Shows input dialog, asking for users name, and sets username
     */
    private void showUserNameDialog(){
        final Dialog userDialog = new Dialog(this);
        userDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        userDialog.setContentView(R.layout.dialog_username);
        userDialog.setCancelable(false);
        Button okButton = (Button) userDialog.findViewById(R.id.dialogButtonOK);
        final EditText userInput = (EditText) userDialog.findViewById(R.id.user_dialog_input);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userInputString = userInput.getText().toString();
                Log.d(TAG, "userinput: " + userInputString);
                onInputOkClick(userDialog, userInputString);
            }
        });
        userDialog.show();
    }

    /**
     * Receives Placemark and distance to nearest letter, sets global variables and
     * adapts view.
     * @param nearestPlaceMark
     * @param nearestDistance
     */
    public void setNearestLetter(PlaceMark nearestPlaceMark, double nearestDistance) {
        this.nearestDistance = nearestDistance;
        nearestLetter = nearestPlaceMark;
        setNavHeader();
    }

    /**
     * Adapt view for nearest letter
     */
    private void setNavHeader() {
        View headerView = navigationView.getHeaderView(0);
        TextView naviUserName = (TextView) headerView.findViewById(R.id.user_name);
        naviUserName.setText("Welcome, " + userName + "!");

        //Set navigation header only, when nearest letter is less than 10km away
        if (nearestDistance < 10000 && nearestLetter != null) {
            TextView greeting = (TextView) headerView.findViewById(R.id.nearestLetter);
            greeting.setText("Nearest Letter is " + nearestLetter.letter.toUpperCase()
                    + " and " + ((int) nearestDistance) + " m away.");

            int id = getResources().getIdentifier(nearestLetter.letter, "drawable", getPackageName());
            ImageView letterIcon = (ImageView) headerView.findViewById(R.id.letter_icon);
            letterIcon.setImageResource(id);
        } else {
            //set mystery marker
            TextView greeting = (TextView) headerView.findViewById(R.id.nearestLetter);
            greeting.setText("Nearest Letter is too far away.");

            int id = getResources().getIdentifier("mysterymarkerpng", "drawable", getPackageName());
            ImageView letterIcon = (ImageView) headerView.findViewById(R.id.letter_icon);
            letterIcon.setImageResource(id);
            letterIcon.setCropToPadding(true);
        }
    }

    /**
     * Reads file from raw folder to a hashset.
     * @throws IOException
     */
    private void readDictionaryToHashSet() throws IOException {
        Log.d(TAG, "read dictionary to HashSet");
        //read from dictionary.txt in raw folder
        BufferedReader reader;
        InputStream is = this.getResources().openRawResource(R.raw.dictionary);
        dictionary = new HashSet<>();

        if (is != null) {
            Log.d(TAG, "dictionary.txt found");
            reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            int counter = 0;
            while (line != null) {
                //add each word to hashset
                dictionary.add(line.toLowerCase());
                line = reader.readLine();
            }
            dictionaryReady = true;
            Log.d(TAG, "Dictionay ready");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Log.d(TAG, String.valueOf(id));

        if (id == R.id.nav_nearest_letter) {
            Log.d(TAG, "map");

            //set values, so map can be updated accordingly
            mainFragment = new MainFragment();
            mainFragment.markers = markers;
            mainFragment.markerReady = true;

            //check if selected fragment already opened
            if (mainFragment.isVisible()) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    mainFragment,
                    mainFragment.getTag()
            ).addToBackStack(mainFragment.getTag()).commit();

            collectionAllowed = true;


        } else if (id == R.id.nav_build_words) {
            Log.d(TAG, "start build");
            collectionAllowed = false;
            buildWordsFragment = new BuildWordsFragment();

            //check if selected fragment already opened
            if (buildWordsFragment.isVisible()) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    buildWordsFragment,
                    buildWordsFragment.getTag()
            ).addToBackStack(buildWordsFragment.getTag()).commit();


        } else if (id == R.id.nav_high_scores) {
            Log.d(TAG, "start high");
            collectionAllowed = false;
            HighScoreFragment highScoreFragment = new HighScoreFragment();

            //check if selected fragment already opened
            if (highScoreFragment.isVisible()) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    highScoreFragment,
                    highScoreFragment.getTag()
            ).addToBackStack(highScoreFragment.getTag()).commit();


        } else if (id == R.id.nav_dictionary) {
            Log.d(TAG, "start dictionary");
            collectionAllowed = false;
            DictionaryFragment dictionaryFragment = new DictionaryFragment();

            //check if selected fragment already opened
            if (dictionaryFragment.isVisible()) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    dictionaryFragment,
                    dictionaryFragment.getTag()
            ).addToBackStack(dictionaryFragment.getTag()).commit();


        } else if (id == R.id.nav_help) {
            Log.d(TAG, "start help");
            collectionAllowed = false;
            HelpFragment helpFragment = new HelpFragment();

            //check if selected fragment already opened
            if (helpFragment.isVisible()) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    helpFragment,
                    helpFragment.getTag()
            ).addToBackStack(helpFragment.getTag()).commit();

        } else if (id == R.id.nav_close) {
            //close App
            this.finishAffinity();

        } else {
            Log.d(TAG, "map");
            mainFragment = new MainFragment();
            mainFragment.markers = markers;
            mainFragment.markerReady = true;

            //check if selected fragment already opened
            if (mainFragment.isVisible()) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.relativelayout_for_fragment,
                    mainFragment,
                    mainFragment.getTag()
            ).addToBackStack(mainFragment.getTag()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_navigation);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    /**
     * Called when Build Word Button is clicked and loads the corresponding fragment
     * @param v
     */
    public void onBuildWordsPressed(View v) {
        Log.d(TAG, "Button start build pressed");
        buildWordsFragment = new BuildWordsFragment();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(
                R.id.relativelayout_for_fragment,
                buildWordsFragment,
                buildWordsFragment.getTag()
        ).addToBackStack(buildWordsFragment.getTag()).commit();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        //get the permission
        checkLocationPermissions();

        if (checkLocationPermissionGranted()) {
            getCurrentLocation();
        }
    }

    private void checkLocationPermissions() {

        boolean granted = checkLocationPermissionGranted();
        if (!granted) {
            //if permission not granted
            Log.d(TAG, "request permission 2");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_ACCESS_FINE_LOCATION);
        } else if(granted){
            return;
        } else {
            //show explanation
            Toast.makeText(this, "Please permit Grabble to access the devices location and start again.", Toast.LENGTH_LONG).show();
            //close app
            this.finishAffinity();
        }
    }

    private void getCurrentLocation() {
        Log.d(TAG, "Permission granted");

        if (checkLocationPermissionGranted()) {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        } else {
            //checkLocationPermissions();
            Log.d(TAG, "Permission not granted");
        }
        // Begin polling for new location updates.
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        // Create the location request
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        // Request location updates if permission is granted
        if (checkLocationPermissionGranted()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    private boolean checkLocationPermissionGranted() {
        //check if permission is not granted
        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.d(TAG, "Permission not granted");
            return false;
        } else {
            //permission  granted
            Log.d(TAG, "permission granted");
            return true;
        }
    }

    /**
     * calculates distance between two points
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    private double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double latA = Math.toRadians(lat1);
        double lonA = Math.toRadians(lon1);
        double latB = Math.toRadians(lat2);
        double lonB = Math.toRadians(lon2);
        double cosAng = (Math.cos(latA) * Math.cos(latB) * Math.cos(lonB - lonA)) +
                (Math.sin(latA) * Math.sin(latB));
        double ang = Math.acos(cosAng);
        double dist = ang * 6371;
        return dist * 1000;
    }

    /**
     * Called when location changes. Updates global variable, if collection is allowed, checks if letters are near,
     * keeps track of users walked distance and checks if a reward should be awarded.
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location Changed");

        if (location != null) {
            Log.d(TAG, "Last location not null.");

            // set global variable
            lastLocation.setLatitude(location.getLatitude());
            lastLocation.setLongitude(location.getLongitude());

            if (collectionAllowed) {
                //calculate walked distance
                double walkedDistance = getDistance(lastLocation.getLatitude(), lastLocation.getLongitude(),
                        location.getLatitude(), location.getLongitude());
                distance += walkedDistance;

                //check if new reward should be awarded
                if (newRewardUnlocked()) {
                    Log.d(TAG, "New reward unlocked");

                    rewardUnlocked++;
                    int number = calculateNumberOfRewards();
                    int whichReward = calculateWhichReward();
                    addRewardToSpecialCount(number, whichReward);

                    showRewardDialog(number, whichReward);
                }

                mainFragment.updateProgress();

                //Check if letter can be collected
                if (!userName.equals("") && mainFragment != null) {
                    boolean anyLettersFound = false;
                    double nearestDistance = 1000000000;
                    PlaceMark nearestPlaceMark = null;

                    Log.d(TAG, "Start collection");
                    for (PlaceMark placeMark : markers) {

                        //get distance
                        Location markerLocation = new Location("placemark");
                        markerLocation.setLatitude(placeMark.latitude);
                        markerLocation.setLongitude(placeMark.longitude);
                        double distance = getDistance(placeMark.latitude, placeMark.longitude,
                                lastLocation.getLatitude(), lastLocation.getLongitude());

                        //find the points that are not more than the normal radius away
                        if (distance <= NORMAL_RADIUS && !visitedPlaceMarks.contains(placeMark.id)) {
                            Log.d(TAG, "Distance to " + placeMark.id + " is " + String.valueOf(distance));
                            anyLettersFound = true;

                            //add to letter count
                            Log.d(TAG, "Add to letter count");
                            int index = alphabet.indexOf(placeMark.letter);
                            letterCount[index] = letterCount[index] + 1;
                            lettersEverCollected++;
                            mainFragment.updateLetterCollected(lettersEverCollected);

                            //show dialog for each
                            Log.d(TAG, "Show dialog for collected letter");
                            showLetterCollectedDialog(placeMark.letter);

                            //delete from markers or add to already collected markers
                            placeMark.setVisited(true);
                            visitedPlaceMarks.add(placeMark.id);
                        }

                        //check for nearest letter
                        if (distance < nearestDistance && !visitedPlaceMarks.contains(placeMark.id)) {
                            nearestDistance = distance;
                            nearestPlaceMark = placeMark;
                        }
                    }

                    // call delete Marker after going through the complete list of markers
                    if (anyLettersFound) {
                        mainFragment.deleteMarker();
                    }

                    if (nearestPlaceMark != null && nearestDistance < 1000000000) {
                        setNearestLetter(nearestPlaceMark, nearestDistance);
                    }
                }
            }
        }
    }

    /**
     * Shows dialog for earned reward
     * @param number
     * @param whichReward
     */
    public void showRewardDialog(int number, int whichReward) {
        Log.d(TAG, "Show reward dialog");

        final Dialog rewardDialog = new Dialog(this);
        rewardDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rewardDialog.setContentView(R.layout.dialog_reward);
        rewardDialog.setCancelable(false);

        TextView specialNumberView = (TextView) rewardDialog.findViewById(R.id.special_number);
        specialNumberView.setText(String.valueOf(number) + " x");

        if (whichReward == 0) {
            //Double Letter Star
            int id = getResources().getIdentifier("blue_star", "drawable", this.getPackageName());
            ImageView icon = (ImageView) rewardDialog.findViewById(R.id.star_icon);
            icon.setImageBitmap(decodeBitmapFromResource(getResources(), id, 120, 120));

            int stringId;
            if (number > 1) {
                stringId = getResources().getIdentifier("double_letter_sing", "string", this.getPackageName());
            } else {
                stringId = getResources().getIdentifier("double_letter_plur", "string", this.getPackageName());
            }
            TextView body = (TextView) rewardDialog.findViewById(R.id.reward_body_2);
            body.setText(stringId);

        } else if (whichReward == 1) {
            //Triple Letter Star
            int id = getResources().getIdentifier("yellow_star", "drawable", this.getPackageName());
            ImageView icon = (ImageView) rewardDialog.findViewById(R.id.star_icon);
            icon.setImageBitmap(decodeBitmapFromResource(getResources(), id, 120, 120));

            int stringId;
            if (number > 1) {
                stringId = getResources().getIdentifier("triple_letter_sing", "string", this.getPackageName());
            } else {
                stringId = getResources().getIdentifier("triple_letter_plur", "string", this.getPackageName());
            }
            TextView body = (TextView) rewardDialog.findViewById(R.id.reward_body_2);
            body.setText(stringId);

        } else if (whichReward == 2) {
            //Double Word Star
            int id = getResources().getIdentifier("red_star", "drawable", this.getPackageName());
            ImageView icon = (ImageView) rewardDialog.findViewById(R.id.star_icon);
            icon.setImageBitmap(decodeBitmapFromResource(getResources(), id, 120, 120));

            int stringId;
            if (number > 1) {
                stringId = getResources().getIdentifier("double_word_sing", "string", this.getPackageName());
            } else {
                stringId = getResources().getIdentifier("double_word_plur", "string", this.getPackageName());
            }
            TextView body = (TextView) rewardDialog.findViewById(R.id.reward_body_2);
            body.setText(stringId);

        } else if (whichReward == 3) {
            //Triple Word Star
            int id = getResources().getIdentifier("green_star", "drawable", this.getPackageName());
            ImageView icon = (ImageView) rewardDialog.findViewById(R.id.star_icon);
            icon.setImageBitmap(decodeBitmapFromResource(getResources(), id, 120, 120));

            int stringId;
            if (number > 1) {
                stringId = getResources().getIdentifier("triple_word_sing", "string", this.getPackageName());
            } else {
                stringId = getResources().getIdentifier("triple_word_plur", "string", this.getPackageName());
            }
            TextView body = (TextView) rewardDialog.findViewById(R.id.reward_body_2);
            body.setText(stringId);
        }
        Button okButton = (Button) rewardDialog.findViewById(R.id.dialogButtonOK);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewardDialog.dismiss();
            }
        });
        rewardDialog.show();
    }

    //method to resize Bitmaps
    private Bitmap decodeBitmapFromResource(Resources res, int resId,
                                            int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
    //method to resize Bitmaps
    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    /**
     * calculate if new result should be awarded
     */
    private boolean newRewardUnlocked() {
        Log.d(TAG, "Reward unlocked? new value: " + String.valueOf(((int) distance) / mainFragment.MAX_PROGRESS) +
                "old value: " + String.valueOf(rewardUnlocked));
        return ((int) distance) / mainFragment.MAX_PROGRESS > rewardUnlocked;
    }

    /**
     * Add received reward to count
     * @param number
     * @param whichReward
     */
    private void addRewardToSpecialCount(int number, int whichReward) {
        specialCount[whichReward] += number;
    }

    /**
     * @return current type of reward
     */
    private int calculateWhichReward() {
        return (rewardUnlocked - 1) % specials.length;
    }

    /**
     * @return current round of rewards
     */
    private int calculateNumberOfRewards() {
        return 1 + ((rewardUnlocked - 1) / specials.length);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted
                    if (googleApiClient != null && googleApiClient.isConnected()) {
                        getCurrentLocation();
                    }
                } else {
                    // permission denied
                    Toast.makeText(this, "Please permit Grabble to access the devices location and start again.", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
                return;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Network lost. Please re-connect.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart()");

        if (googleApiClient != null && !googleApiClient.isConnected()) {
            googleApiClient.connect();
            Log.d(TAG, "connected");
        }
        super.onStart();
    }

    protected void onRestart() {
        if (!googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
        super.onRestart();
    }

    protected void onResume() {
        if (googleApiClient.isConnected()) {
            getCurrentLocation();
        } else if (googleApiClient != null) {
            googleApiClient.connect();
        }
        if(mainFragment != null){
            mainFragment.initialiseMapAndMarker(true);
        }
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop()");
        saveToSharedPreferences();

        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    private void saveToSharedPreferences() {

        //Save data to shared preferences that is needed for gameflow
        SharedPreferences.Editor editor = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE).edit();
        saveToSharedPreferenceString(userName, "userName", editor);
        saveToSharedPreferenceString(currentWord, "currentWord", editor);
        saveToSharedPreferenceInt(totalPoints, "totalPoints", editor);
        saveToSharedPreferenceInt(lettersEverCollected, "lettersCollected", editor);
        saveToSharedPreferenceDouble(distance, "distance", editor);
        saveToSharedPreferenceWordInfoList(userWords, "userWords", editor);
        saveToSharedPreferenceArray(letterCount, "letterCount", editor);
        saveToSharedPreferenceArray(specialCount, "specialCount", editor);
        saveToSharedPreferencePlaceMarkList(new ArrayList<String>(visitedPlaceMarks), "visitedPlaceMarks", editor);
        saveToSharedPreferenceString(String.valueOf(doubleLetterClicked), "doubleLetterClicked", editor);
        saveToSharedPreferenceString(String.valueOf(tripleLetterClicked), "tripleLetterClicked", editor);
        saveToSharedPreferenceString(String.valueOf(doubleWordClicked), "doubleWordClicked", editor);
        saveToSharedPreferenceString(String.valueOf(tripleWordClicked), "tripleWordClicked", editor);
        saveToSharedPreferenceInt(rewardUnlocked, "rewardUnlocked", editor);
        saveToSharedPreferencesMarkers(markers, "markers", editor);
    }

    private void restoreFromSharedPreferences() {

        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);

        totalPoints = getFromSharedPreferencesInt("totalPoints", prefs);
        letterCount = getFromSharedPreferencesIntArray("letterCount", prefs, 26);
        currentWord = getFromSharedPreferencesString("currentWord", prefs);
        lettersEverCollected = getFromSharedPreferencesInt("lettersCollected", prefs);
        specialCount = getFromSharedPreferencesIntArray("specialCount", prefs, 4);
        userName = getFromSharedPreferencesString("userName", prefs);
        distance = getFromSharedPreferencesDouble("distance", prefs);
        userWords = getFromSharedPreferencesWordInfo("userWords", prefs);
        visitedPlaceMarks = getFromSharedPreferencesHashSet("visitedPlaceMarks", prefs);
        markers = getFromSharedPreferencesMarkers("markers", prefs);
        doubleLetterClicked = getFromSharedPreferencesBool("doubleLetterClicked", prefs);
        tripleLetterClicked = getFromSharedPreferencesBool("tripleLetterClicked", prefs);
        doubleWordClicked = getFromSharedPreferencesBool("doubleWordClicked", prefs);
        tripleWordClicked = getFromSharedPreferencesBool("tripleWordClicked", prefs);
        rewardUnlocked = getFromSharedPreferencesInt("rewardUnlocked", prefs);

    }

    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection Failed");

        Toast.makeText(this, "Connection Failed. Please retry later.", Toast.LENGTH_LONG).show();
        this.finishAffinity();

    }

    /**
     * Responsible for determining, if a new map needs to be downloaded, and deals with result.
     * @throws IOException
     * @throws XmlPullParserException
     */
    private void getMap() throws IOException, XmlPullParserException {
        Log.d(TAG, "getMap() called");

        //check, if new map needs to be downloaded
        String currentWeekDay = getCurrentLowerCaseWeekDay();
        String fileName = currentWeekDay + "_map.txt";

        //get the file
        String path = this.getFilesDir() + "/" + fileName;
        File file = new File(path);

        //get modification date
        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
        String lastModDate = sd.format(new Date(file.lastModified()));
        String today = sd.format(new Date());

        Log.d(TAG, "Today: " + today + "last modified: " + lastModDate);

        if (!file.exists() || !today.equals(lastModDate)) {

            Log.d(TAG, "todays map does not exist");
            //delete all other map files
            deleteAllMapFiles();

            //start download
            String downloadUrl = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/"
                    + currentWeekDay + ".kml";
            Log.d(TAG, "start download");
            startDownload(downloadUrl);

            visitedPlaceMarks = new HashSet<>();

        } else if(markers.size() == 0){

            try{
                InputStream is = new FileInputStream(file);
                createMarkersFromInputStream(is);
                is.close();
            } catch(IOException e){
                e.printStackTrace();

                Log.e(TAG, "Download file instead");
                //delete all other map files
                deleteAllMapFiles();

                //start download
                String downloadUrl = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/"
                        + currentWeekDay + ".kml";
                Log.d(TAG, "start download");
                startDownload(downloadUrl);
            }

        }
        else {
            Log.d(TAG, "todays map exists");
            createMarkers();
        }
    }

    /**
     * deletes all files from local memory that are connected to maps
     */
    private void deleteAllMapFiles() {

        File dir = this.getFilesDir();
        File file = new File(dir, "sunday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete sunday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        file = new File(dir, "monday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete monday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        file = new File(dir, "tuesday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete tuesday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        file = new File(dir, "wednesday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete wednesday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        file = new File(dir, "thursday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete thursday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        file = new File(dir, "friday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete friday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        file = new File(dir, "saturday_map.txt");
        try {
            boolean deleted = file.delete();
            Log.d(TAG, "delete saturday map");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void createMarkers() {
        if (mainFragment != null) {
            mainFragment.addMarkers(markers);
            collectionAllowed = true;
        }
    }

    private void createMarkersFromInputStream(InputStream input){
        Log.d(TAG, "createMarkersFromString");
        //parse string to placemarks
        try{
            markers = parseFileToXml(input);
            createMarkers();
        } catch (IOException e){
            e.printStackTrace();
        } catch(XmlPullParserException x){
            x.printStackTrace();
        }


    }

    /**
     * Set up parser with imputstream to parse and attributes
     * @param input
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private ArrayList<PlaceMark> parseFileToXml(InputStream input) throws XmlPullParserException, IOException {

        try {
            Log.d(TAG, "Parse xml from file");
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            Log.d(TAG, "Parse xml from file, parser done");

            // set the input for the parser using an InputStreamReader
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(input, null);
            Log.d(TAG, "Parse xml from file, set input");

            return parseXML(parser);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Parses xml to Arraylist of Class PlaceMark
     * @param parser
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private ArrayList<PlaceMark> parseXML(XmlPullParser parser) throws XmlPullParserException, IOException {
        Log.d(TAG, "parseXml into placemarks");
        ArrayList<PlaceMark> markers = null;
        int eventType = parser.getEventType();
        PlaceMark currentMark = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name;
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    //Log.d(TAG, "case: start Document");
                    markers = new ArrayList();
                    break;

                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    //Log.d(TAG, "name: "+ name);

                    if (name.equalsIgnoreCase("Placemark")) {
                        //Log.d(TAG, "new Placemark");
                        currentMark = new PlaceMark();

                    } else if (currentMark != null) {
                        //Log.d(TAG, "currentMark not null");

                        if (name.equalsIgnoreCase("name")) {
                            //Log.d(TAG, "name");
                            currentMark.id = parser.nextText();

                        } else if (name.equalsIgnoreCase("description")) {
                            //Log.d(TAG, "description");
                            currentMark.letter = parser.nextText().toLowerCase();

                        } else if (name.equalsIgnoreCase("Point")) {
                            //Log.d(TAG, "Point");
                            //break;

                        } else if (name.equalsIgnoreCase("coordinates")) {
                            //Log.d(TAG, "Coordiantes");
                            String coord = parser.nextText();
                            String[] splitCoord = coord.split(",");
                            currentMark.latitude = Double.parseDouble(splitCoord[1]);
                            currentMark.longitude = Double.parseDouble(splitCoord[0]);
                        }

                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (currentMark != null) {
                        //Log.d(TAG, "currentMark.letter: "+ currentMark.letter);
                        markers.add(currentMark);
                    }
            }
            eventType = parser.next();
        }

        //full list of markers
        return (markers);
    }

    private String getCurrentLowerCaseWeekDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                return "sunday";

            case Calendar.MONDAY:
                return "monday";

            case Calendar.TUESDAY:
                return "tuesday";

            case Calendar.WEDNESDAY:
                return "wednesday";

            case Calendar.THURSDAY:
                return "thursday";

            case Calendar.FRIDAY:
                return "friday";

            case Calendar.SATURDAY:
                return "saturday";

        }
        Log.d(TAG, "no weekday found from calender");
        return "sunday";
    }

    /**
     * Calls async download task
     * @param downloadUrl
     */
    private void startDownload(String downloadUrl) {
        Log.d(TAG, "startDownload called");
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.d(TAG, "New DownloadWebpageTask execute");
            new DownloadWebpageTask().execute(downloadUrl);
        } else {
            Toast.makeText(this, "No network connection available ", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Processes input from textbox
     * @param dialog
     * @param input
     */
    public void onInputOkClick(Dialog dialog, String input) {
        //get input from Editext from Dialog
        Log.d(TAG, "Ok clicked");
        if (input.length() >= 5 && input.length() < 13) {
            userName = input;
            dialog.dismiss();
        } else {
            Toast.makeText(this, "Please enter more than 5 and no more than 12 letters.", Toast.LENGTH_LONG).show();
            return;
        }
    }

    /**
     * Saves string result in internal memory with the file name "current day" + "_map.txt"
     * @param result
     * @throws IOException
     */
    private void saveInFile(String result) throws IOException {
        Log.d(TAG, "saveInFile()");
        String currentDay = getCurrentLowerCaseWeekDay();
        String fileName = currentDay + "_map.txt";

        try {
            Log.d(TAG, "write in fileOutputStream");
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            fos.write(result.getBytes());
            fos.close();
            Log.d(TAG, "fileOutputStream closed");
        } catch (IOException e) {
            Log.e(TAG, "Could not write to internal memory");
            e.printStackTrace();
        }
    }

    /**
     * Gets content from Url and returns a String.
     * @param downloadUrl
     * @return
     * @throws IOException
     */
    private String downloadUrl(String downloadUrl) throws IOException {
        Log.d(TAG, "downloadUrl called");
        InputStream is = null;

        try {
            Log.d(TAG, "try create connection");
            URL url = new URL(downloadUrl);
            HttpURLConnection conn = (HttpURLConnection) url.
                    openConnection();
            conn.setReadTimeout(100000);
            conn.setConnectTimeout(150000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            //Starts the query
            conn.connect();

            int response = conn.getResponseCode();

            Log.d(TAG, "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            return readLongInput(is);

        } catch (IOException e) {
            Log.e(TAG, "Connection not possible");
            e.printStackTrace();
            return "";
        }

        // Make sure that the InputStream is closed
        finally {
            if (is != null) {
                is.close();
            }
        }
    }

    /**
     * Converts InputStream to string.
     * @param stream
     * @return
     * @throws IOException
     */
    private String readLongInput(InputStream stream) throws IOException {
        Log.d(TAG, "read Long Input");

        StringBuffer data = new StringBuffer("");
        try {
            InputStreamReader is = new InputStreamReader(stream);
            BufferedReader buffreader = new BufferedReader(is);

            String readString = buffreader.readLine();
            while (readString != null) {
                data.append(readString + "\n");
                readString = buffreader.readLine();
            }
            is.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return data.toString();
    }

    //For debug
    private void printLongString(String s) throws IOException {
        Log.d(TAG, "print long String");
        // convert String into InputStream
        InputStream is = new ByteArrayInputStream(s.getBytes());

        // read it with BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        while ((line = br.readLine()) != null) {
            Log.d(TAG, line);
        }

        br.close();
    }

    /**
     * Calculates value of word, taking special powers into account.
     * @param word
     * @return
     */
    public int calculateWordPoints(String word) {
        word = word.toLowerCase();
        int wordPoints = 0;
        int largestValue = 0;

        if (word.length() == 0) {
            return wordPoints;
        }

        //get value of each letter
        for (int i = 0; i < word.length(); i++) {
            String current = Character.toString(word.charAt(i));

            int index = alphabet.indexOf(current);
            Log.d(TAG, "letterValue of " + current + " is " + letterValue[index]);
            wordPoints += letterValue[index];
            Log.d(TAG, "wordPoints: " + wordPoints);

            if (letterValue[index] > largestValue) {
                largestValue = letterValue[index];
            }
        }

        //include special powers
        if (doubleLetterClicked) {
            wordPoints += largestValue;
        } else if (tripleLetterClicked) {
            wordPoints += 2 * largestValue;
        } else if (doubleWordClicked) {
            wordPoints = 2 * wordPoints;
        } else if (tripleWordClicked) {
            wordPoints = 3 * wordPoints;
        }

        return wordPoints;
    }

    /**
     * Processe click on Delete button in BuildWordsFragment, which resets the view and variables
     * @param v
     */
    public void onDeleteWordClicked(View v) {

        if (doubleLetterClicked) {

            doubleLetterClicked = false;
            specialCount[0]++;

            buildWordsFragment.changeBackground(specials[0], false);

        } else if (tripleLetterClicked) {
            tripleLetterClicked = false;
            specialCount[1]++;

            buildWordsFragment.changeBackground(specials[1], false);

        } else if (doubleWordClicked) {
            doubleWordClicked = false;
            specialCount[2]++;

            buildWordsFragment.changeBackground(specials[2], false);

        } else if (tripleWordClicked) {
            tripleWordClicked = false;
            specialCount[3]++;

            buildWordsFragment.changeBackground(specials[3], false);

        }
        buildWordsFragment.updateSpecialCount();
        buildWordsFragment.deleteWord(true);
    }

    /**
     * Processes click of Check button in BuildWordsFragment
     * @param v
     * @throws IOException
     */
    public void onCheckWordClicked(View v) throws IOException {
        if (dictionaryReady && currentWord.length() == 7 && isWordInDictionary(currentWord)) {
            Log.d(TAG, "Word accepted");
            int wordPoints = calculateWordPoints(currentWord);

            //update user dictionary
            WordInfo wordInfo = new WordInfo(currentWord, wordPoints, new Date());
            userWords.add(wordInfo);
            sortUserWords();
            Log.d(TAG, "New Word added: " + currentWord + " and length of userWords: " + userWords.size());

            showCongratDialog(currentWord);

            //update totalpoints
            totalPoints = totalPoints + wordPoints;
            buildWordsFragment.updateTotalPoints();

            //reset global values and views
            doubleLetterClicked = false;
            tripleLetterClicked = false;
            doubleWordClicked = false;
            tripleWordClicked = false;
            buildWordsFragment.changeBackground("", false);
            buildWordsFragment.deleteWord(false);
        }
    }

    /**
     * Sorts global ArrayList alphabetically by the attribute word of WordInfo
     */
    private void sortUserWords() {
        if (userWords.size() > 0) {
            Collections.sort(userWords, new Comparator<WordInfo>() {
                @Override
                public int compare(final WordInfo object1, final WordInfo object2) {
                    return object1.word.compareTo(object2.word);
                }
            });
        }
    }

    /**
     * Opens dialog with word
     * @param word
     */
    private void showCongratDialog(String word) {
        final Dialog congratDialog = new Dialog(this);
        congratDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        congratDialog.setContentView(R.layout.dialog_congrat_word);
        congratDialog.setCancelable(false);
        TextView wordView = (TextView) congratDialog.findViewById(R.id.checked_word);
        wordView.setText(word);
        Button okButton = (Button) congratDialog.findViewById(R.id.dialogButtonOK);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                congratDialog.dismiss();
            }
        });
        congratDialog.show();
    }

    /**
     * Opens dialog with letter image
     * @param currentLetter
     */
    private void showLetterCollectedDialog(String currentLetter) {

        //create dialog
        final Dialog congratDialog = new Dialog(this);
        congratDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        congratDialog.setContentView(R.layout.dialog_letter_collected);
        congratDialog.setCancelable(false);

        //set imagesource to current letter
        currentLetter = currentLetter.toLowerCase();
        int id = getResources().getIdentifier(currentLetter, "drawable", this.getPackageName());
        ImageView icon = (ImageView) congratDialog.findViewById(R.id.letter_icon);
        icon.setImageResource(id);

        //set up dismiss button
        Button okButton = (Button) congratDialog.findViewById(R.id.dialogButtonOK);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                congratDialog.dismiss();
            }
        });
        congratDialog.show();
    }

    /**
     * Checks if word is valid
     * @param word
     * @return
     * @throws IOException
     */
    private boolean isWordInDictionary(String word) throws IOException {
        Log.d(TAG, "check Word in Dictionary");
        if (dictionary != null) {
            Log.d(TAG, "word in dictionary: " + dictionary.contains(word));
            return dictionary.contains(word);
        } else {
            Log.d(TAG, "dictionary is null");
            return false;
        }
    }

    /**
     * Processes click on image of letter and updates app
     * @param v
     */
    public void chooseLetter(View v) {
        if (currentWord.length() >= 7) {
            return;
        } else {

            int index = currentWord.length() - 1;

            switch (v.getId()) {
                case R.id.a_icon: {

                    Log.d(TAG, "case: a");

                    int countIndex = alphabet.indexOf("a");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("a", index + 1);
                        currentWord = currentWord + "a";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("a", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;

                }

                case R.id.b_icon: {

                    Log.d(TAG, "case: b");

                    int countIndex = alphabet.indexOf("b");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("b", index + 1);
                        currentWord = currentWord + "b";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("b", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.c_icon: {

                    Log.d(TAG, "case: c");

                    int countIndex = alphabet.indexOf("c");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("c", index + 1);
                        currentWord = currentWord + "c";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("c", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.d_icon: {

                    Log.d(TAG, "case: d");

                    int countIndex = alphabet.indexOf("d");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("d", index + 1);
                        currentWord = currentWord + "d";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("d", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.e_icon: {

                    Log.d(TAG, "case: e");

                    int countIndex = alphabet.indexOf("e");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("e", index + 1);
                        currentWord = currentWord + "e";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("e", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.f_icon: {

                    Log.d(TAG, "case: f");

                    int countIndex = alphabet.indexOf("f");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("f", index + 1);
                        currentWord = currentWord + "f";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("f", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.g_icon: {

                    Log.d(TAG, "case: g");

                    int countIndex = alphabet.indexOf("g");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("g", index + 1);
                        currentWord = currentWord + "g";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("g", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.h_icon: {

                    Log.d(TAG, "case: h");

                    int countIndex = alphabet.indexOf("h");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("h", index + 1);
                        currentWord = currentWord + "h";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("h", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.i_icon: {

                    Log.d(TAG, "case: i");

                    int countIndex = alphabet.indexOf("i");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("i", index + 1);
                        currentWord = currentWord + "i";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("i", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.j_icon: {

                    Log.d(TAG, "case: j");

                    int countIndex = alphabet.indexOf("j");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("j", index + 1);
                        currentWord = currentWord + "j";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("j", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.k_icon: {

                    Log.d(TAG, "case: k");

                    int countIndex = alphabet.indexOf("k");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("k", index + 1);
                        currentWord = currentWord + "k";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("k", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.l_icon: {

                    Log.d(TAG, "case: l");

                    int countIndex = alphabet.indexOf("l");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("l", index + 1);
                        currentWord = currentWord + "l";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("l", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.m_icon: {

                    Log.d(TAG, "case: m");

                    int countIndex = alphabet.indexOf("m");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("m", index + 1);
                        currentWord = currentWord + "m";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("m", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.n_icon: {

                    Log.d(TAG, "case: n");

                    int countIndex = alphabet.indexOf("n");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("n", index + 1);
                        currentWord = currentWord + "n";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("n", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.o_icon: {

                    Log.d(TAG, "case: o");

                    int countIndex = alphabet.indexOf("o");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("o", index + 1);
                        currentWord = currentWord + "o";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("o", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.p_icon: {

                    Log.d(TAG, "case: o");

                    int countIndex = alphabet.indexOf("p");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("p", index + 1);
                        currentWord = currentWord + "p";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("p", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.q_icon: {

                    Log.d(TAG, "case: q");

                    int countIndex = alphabet.indexOf("q");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("q", index + 1);
                        currentWord = currentWord + "q";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("q", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.r_icon: {

                    Log.d(TAG, "case: o");

                    int countIndex = alphabet.indexOf("r");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("r", index + 1);
                        currentWord = currentWord + "r";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("r", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.s_icon: {

                    Log.d(TAG, "case: s");

                    int countIndex = alphabet.indexOf("s");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("s", index + 1);
                        currentWord = currentWord + "s";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("s", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.t_icon: {

                    Log.d(TAG, "case: t");

                    int countIndex = alphabet.indexOf("t");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("t", index + 1);
                        currentWord = currentWord + "t";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("t", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.u_icon: {

                    Log.d(TAG, "case: u");

                    int countIndex = alphabet.indexOf("u");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("u", index + 1);
                        currentWord = currentWord + "u";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("u", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.v_icon: {

                    Log.d(TAG, "case: v");

                    int countIndex = alphabet.indexOf("v");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("v", index + 1);
                        currentWord = currentWord + "v";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("v", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.w_icon: {

                    Log.d(TAG, "case: w");

                    int countIndex = alphabet.indexOf("w");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("w", index + 1);
                        currentWord = currentWord + "w";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("w", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.x_icon: {

                    Log.d(TAG, "case: x");

                    int countIndex = alphabet.indexOf("x");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("x", index + 1);
                        currentWord = currentWord + "x";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("x", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.y_icon: {

                    Log.d(TAG, "case: y");

                    int countIndex = alphabet.indexOf("y");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("y", index + 1);
                        currentWord = currentWord + "y";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("y", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                case R.id.z_icon: {

                    Log.d(TAG, "case: z");

                    int countIndex = alphabet.indexOf("z");
                    int currentCount = letterCount[countIndex];

                    if (currentCount > 0) {
                        //replace blank tile by correct letter
                        buildWordsFragment.addLetter("z", index + 1);
                        currentWord = currentWord + "z";

                        //decrement count in lettercount
                        letterCount[countIndex]--;

                        //update keyboard view
                        buildWordsFragment.updateSingleLetterCount("z", countIndex);

                        //calculate new wordPoints
                        buildWordsFragment.updateWordPoints();

                    }
                    break;
                }

                default: {
                    break;
                }
            }
        }
        return;
    }

    /**
     * Processes click on image of super power star
     * @param v
     */
    public void superPressed(View v) {

        if (doubleLetterClicked || tripleLetterClicked || doubleWordClicked || tripleWordClicked) {
            return;
        } else {
            switch (v.getId()) {
                case R.id.double_letter_star: {
                    Log.d(TAG, "case: double_letter_star");

                    if (specialCount[0] < 1) {
                        break;
                    }

                    doubleLetterClicked = true;
                    Log.d(TAG, "SpecialCount[0]: " + specialCount[0]);
                    specialCount[0]--;
                    Log.d(TAG, "SpecialCount[0]: " + specialCount[0]);

                    if (buildWordsFragment != null) {
                        buildWordsFragment.updateWordPoints();
                        buildWordsFragment.changeBackground(specials[0], true);
                        buildWordsFragment.updateSpecialCount();
                    }
                    break;
                }

                case R.id.triple_letter_star: {
                    Log.d(TAG, "case: triple_letter_star");

                    if (specialCount[1] < 1) {
                        break;
                    }

                    tripleLetterClicked = true;
                    Log.d(TAG, "SpecialCount[1]: " + specialCount[1]);
                    specialCount[1]--;
                    Log.d(TAG, "SpecialCount[1]: " + specialCount[1]);

                    if (buildWordsFragment != null) {
                        buildWordsFragment.updateWordPoints();
                        buildWordsFragment.changeBackground(specials[1], true);
                        buildWordsFragment.updateSpecialCount();
                    }
                    break;
                }

                case R.id.double_word_star: {
                    Log.d(TAG, "case: double_word_star");

                    if (specialCount[2] < 1) {
                        break;
                    }

                    doubleWordClicked = true;
                    Log.d(TAG, "SpecialCount[2]: " + specialCount[2]);
                    specialCount[2]--;
                    Log.d(TAG, "SpecialCount[2]: " + specialCount[2]);

                    if (buildWordsFragment != null) {
                        buildWordsFragment.updateWordPoints();
                        buildWordsFragment.changeBackground(specials[2], true);
                        buildWordsFragment.updateSpecialCount();
                    }
                    break;
                }

                case R.id.triple_word_star: {
                    Log.d(TAG, "case: triple_word_star");

                    if (specialCount[3] < 1) {
                        break;
                    }

                    tripleWordClicked = true;
                    Log.d(TAG, "SpecialCount[3]: " + specialCount[3]);
                    specialCount[3]--;
                    Log.d(TAG, "SpecialCount[3]: " + specialCount[3]);

                    if (buildWordsFragment != null) {
                        buildWordsFragment.updateWordPoints();
                        buildWordsFragment.changeBackground(specials[3], true);
                        buildWordsFragment.updateSpecialCount();
                    }
                    break;
                }

                default: {
                    break;
                }
            }
        }
    }

    private void saveToSharedPreferenceString(String data, String fileName, SharedPreferences.Editor editor) {
        editor.putString(fileName, data);
        editor.apply();
    }

    private void saveToSharedPreferenceInt(int data, String fileName, SharedPreferences.Editor editor) {
        editor.putInt(fileName, data);
        editor.apply();
    }

    private void saveToSharedPreferenceDouble(double data, String fileName, SharedPreferences.Editor editor) {
        editor.putLong(fileName, Double.doubleToRawLongBits(data));
    }

    private void saveToSharedPreferenceWordInfoList(ArrayList<WordInfo> words, String fileName, SharedPreferences.Editor editor) {
        Gson gson = new Gson();
        String userWords = gson.toJson(words);
        saveToSharedPreferenceString(userWords, fileName, editor);
    }

    private void saveToSharedPreferencePlaceMarkList(ArrayList<String> marks, String fileName, SharedPreferences.Editor editor) {
        Gson gson = new Gson();
        String placemark = gson.toJson(marks);
        saveToSharedPreferenceString(placemark, fileName, editor);
    }

    private void saveToSharedPreferencesMarkers(ArrayList<PlaceMark> placemarks, String fileName, SharedPreferences.Editor editor) {
        Gson gson = new Gson();
        String placemark = gson.toJson(placemarks);
        saveToSharedPreferenceString(placemark, fileName, editor);
    }

    private void saveToSharedPreferenceArray(int[] array, String fileName, SharedPreferences.Editor editor) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            str.append(array[i]).append(",");
        }
        saveToSharedPreferenceString(str.toString(), fileName, editor);
    }

    private String getFromSharedPreferencesString(String fileName, SharedPreferences prefs) {
        String restoredText = prefs.getString(fileName, null);
        if (restoredText != null) {
            return restoredText;
        } else {
            return "";
        }
    }

    private int getFromSharedPreferencesInt(String fileName, SharedPreferences prefs) {
        int restoredInt = prefs.getInt(fileName, 0);
        return restoredInt;
    }

    private double getFromSharedPreferencesDouble(String fileName, SharedPreferences prefs) {
        return Double.longBitsToDouble(prefs.getLong(fileName, Double.doubleToLongBits(0.0)));
    }

    private ArrayList<WordInfo> getFromSharedPreferencesWordInfo(String fileName, SharedPreferences prefs) {
        Gson gson = new Gson();
        String json = prefs.getString(fileName, "");
        if (json.equals("")) {
            return new ArrayList<WordInfo>();
        }
        Type type = new TypeToken<ArrayList<WordInfo>>() {
        }.getType();
        ArrayList<WordInfo> userWords = gson.fromJson(json, type);
        return userWords;

    }

    private boolean getFromSharedPreferencesBool(String fileName, SharedPreferences prefs) {
        String restoredText = prefs.getString(fileName, null);
        if (restoredText != null && restoredText.equals("true")) {
            return true;
        } else {
            return false;
        }
    }

    private int[] getFromSharedPreferencesIntArray(String fileName, SharedPreferences prefs, int length) {
        String str = getFromSharedPreferencesString(fileName, prefs);
        //return with zero initialized array
        if (str.equals("")) {
            if (length == 4) {
                return new int[]{0, 0, 0, 0};
            } else if (length == 26) {
                return new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            } else {
                return new int[26];
            }
        }

        StringTokenizer st = new StringTokenizer(str, ",");
        int[] count = new int[length];
        for (int i = 0; i < length; i++) {
            count[i] = Integer.parseInt(st.nextToken());
        }
        return count;
    }

    private HashSet<String> getFromSharedPreferencesHashSet(String fileName, SharedPreferences prefs) {
        Gson gson = new Gson();
        String json = prefs.getString(fileName, "");
        if (json.equals("")) {
            Log.d(TAG, "Return empty hashset");
            return new HashSet<>();
        }
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> visited = gson.fromJson(json, type);
        Log.d(TAG, "visited: " + visited.toString());
        return new HashSet<>(visited);
    }

    private ArrayList<PlaceMark> getFromSharedPreferencesMarkers(String fileName, SharedPreferences prefs) {
        Gson gson = new Gson();
        String json = prefs.getString(fileName, "");
        if (json.equals("")) {
            Log.d(TAG, "Return empty ArrayList");
            return new ArrayList<>();
        }
        Type type = new TypeToken<ArrayList<PlaceMark>>() {
        }.getType();
        ArrayList<PlaceMark> placemarks = gson.fromJson(json, type);
        return placemarks;
    }

    private class DownloadWebpageTask extends AsyncTask<String, Void,
            String> {

        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call : params[0] is the url
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //Save downloaded map in internal storage
            try {
                Log.d(TAG, "save result in file");
                saveInFile(result);
                createMarkersFromInputStream(new ByteArrayInputStream(result.getBytes("UTF-8")));
            } catch (IOException e) {
                Log.e(TAG, "no result from download");
                e.printStackTrace();
            }
        }
    }
}