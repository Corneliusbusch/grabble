package selp.grabble;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

/**
 * Main
 */
public class MainActivity2 extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        drawerLayout = (DrawerLayout) findViewById(R.id.activity_navigation);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);


    }

    public void buildWordsNowClicked(View view) {
        Log.d(TAG, "Build Words Clicked");
        Intent intent = new Intent(MainActivity2.this, BuildWordsActivity.class);
        startActivity(intent);
    }

    public void selectItem(MenuItem item) {
        Intent intent = null;
        int itemId = item.getItemId();
        switch (itemId) {

            case 1:
                Log.d(TAG, "case 1");
                intent = new Intent(this, BuildWordsActivity.class);
                startActivity(intent);
                drawerLayout.closeDrawers();
                break;

            /*case 2:
                intent = new Intent(this, HighScoresActivity.class);
                startActivity(intent);
                drawerLayout.closeDrawers();
                break;*/

            case 3:
                Log.d(TAG, "case 3");
                intent = new Intent(this, DictionaryActivity.class);
                startActivity(intent);
                drawerLayout.closeDrawers();
                break;

            /*case 4:
                intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                drawerLayout.closeDrawers();
                break;*/


            default:
                Log.d(TAG, "itemId :" + itemId);
                intent = new Intent(this, MainActivity2.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                drawerLayout.closeDrawers();
                break;
        }


    }

    //From http://stackoverflow.com/questions/22467516/navigation-drawer-to-switch-between-activities
    /*private class DrawerItemClickListener implements NavigationView.OnNavigationItemSelectedListener  {
        @Override
        public void onNavigationItemSelected(MenuItem item) {
            selectItem(item);
            drawerLayout.closeDrawer(navigationView);
        }

    }*/
}
