package selp.grabble;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import static selp.grabble.MainActivity.MY_ACCESS_FINE_LOCATION;

/**
 * Fragment containing the Map
 * http://www.joellipman.com/articles/google/android/application-development/android-os-add-googlemap-as-fragment.html
 * http://stackoverflow.com/questions/35290974/required-com-google-android-gms-googlemap-found-void
 * http://stackoverflow.com/questions/17326524/change-many-imageview-sources-using-for-loop
 * //http://stackoverflow.com/questions/35718103/how-to-specify-the-size-of-the-icon-on-the-marker-in-google-maps-v2-android
 * //http://stackoverflow.com/questions/14851641/change-marker-size-in-google-maps-api-v2
 */
public class MainFragment extends Fragment implements OnMapReadyCallback {
    public final int MAX_PROGRESS = 500; //Reward every 500m
    //markerReady set on false on initial loading
    public boolean mapReady = false;
    public int currentProgress;
    MapView mMapView;
    ArrayList<PlaceMark> markers;

    //Location of Edinburgh, George Square
    double latEdinburgh = 55.9436;
    double lonEdinburgh = -3.1888;
    boolean markerReady = false;
    private String TAG = this.getClass().getSimpleName();
    private MainActivity activity;
    private GoogleMap googleMap;
    private ProgressBar progressBar;


    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_main, container, false);

        View v = inflater.inflate(R.layout.fragment_main, container,
                false);

        activity = (MainActivity) getActivity();

        //set variable points
        TextView totalPoints = (TextView) v.findViewById(R.id.totalPoints);
        totalPoints.setText(String.valueOf(activity.totalPoints));

        TextView lettersCollected = (TextView) v.findViewById(R.id.letterCollected);
        lettersCollected.setText(String.valueOf(activity.lettersEverCollected));

        //Set up progressbar
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        progressBar.setMax(MAX_PROGRESS);
        updateProgress();

        setUpMap(v, savedInstanceState);

        return v;
    }

    /**
     * increases the number of collected letters
     */
    public void updateLetterCollected(int number) {
        Log.d(TAG, "update letter collected");
        TextView lettersCollected = (TextView) getView().findViewById(R.id.letterCollected);
        lettersCollected.setText(String.valueOf(number));
    }

    /**
     * Initialise MapView
     * @param v
     * @param savedInstanceState
     */
    public void setUpMap(View v, Bundle savedInstanceState) {
        mapReady = false;
        mMapView = (MapView) v.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(this);
        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(activity.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);
    }

    /**
     * Update progressbar
     */
    public void updateProgress() {

        currentProgress = (int) activity.distance % MAX_PROGRESS;
        Log.d(TAG, "current progress: " + currentProgress);
        progressBar.setProgress(currentProgress);
    }

    @Override
    public void onResume() {
        initialiseMapAndMarker(false);
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (mMapView != null) {
            mMapView.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    /**
     * Set up GoogleMap and call method to initialize markers
     * @param map
     */
    @Override
    public void onMapReady(GoogleMap map) {
        Log.d(TAG, "onMapReady()");
        googleMap = map;
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);


        //if permission not granted
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            //show explanation
            Toast.makeText(getContext(), "Please grant the location permissions. " +
                    "Otherwise, you will not be able to use Grabble.", Toast.LENGTH_SHORT).show();

            //Now ask for permission
            Log.d(TAG, "request Permission 1");
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_ACCESS_FINE_LOCATION);
        } else {
            Log.d(TAG, "Permission granted");
            if(googleMap != null){
                googleMap.setMyLocationEnabled(true);
            }
        }

        //now map is ready
        mapReady = true;
        initialiseMapAndMarker(true);

    }

    public void addMarkers(ArrayList<PlaceMark> placeMarks) {

        Log.d(TAG, "addMarkers called");
        markers = placeMarks;

        //now markers are ready
        markerReady = true;
        initialiseMapAndMarker(true);

    }

    /**
     * Adds marker to the map.
     * @param newMap
     */
    public void initialiseMapAndMarker(Boolean newMap) {

        Log.d(TAG, "initialiseMapAndMarker");
        Log.d(TAG, "markerReady: " + markerReady);
        Log.d(TAG, "MapReady: " + mapReady);

        if (!markerReady || !mapReady || googleMap == null) {
            Log.d(TAG, "marker or map not ready yet");
            return;
        }

        //allow collection of letters only when marker are initialised
        activity.collectionAllowed = true;

        //set Edinburgh, George Square as default zoom
        if (newMap) {
            Log.d(TAG, "Move CameraPosition");
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latEdinburgh, lonEdinburgh)).zoom(19).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }

        // create marker icon
        int id = getResources().getIdentifier("mysterymarkerpng", "drawable", activity.getPackageName());

        //resize image to appropriate size
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(id, null);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, 50, 75, false);

        // create marker
        Log.d(TAG, "create marker");
        for (PlaceMark placeMark : markers) {

            if (!activity.visitedPlaceMarks.contains(placeMark.id)) {
                MarkerOptions marker = (new MarkerOptions()).position(
                        new LatLng(placeMark.latitude, placeMark.longitude));

                // Changing marker icon
                marker.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

                googleMap.addMarker(marker);
            }
        }
    }

    public void deleteMarker() {
        if(googleMap != null){
            googleMap.clear();
        }
        initialiseMapAndMarker(false);
    }

    @Override
    public void onStop() {
        activity.collectionAllowed = false;
        super.onStop();
    }
}
