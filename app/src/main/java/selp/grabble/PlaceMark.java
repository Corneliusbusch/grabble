package selp.grabble;

/**
 * Created by nicole on 09.01.17.
 *
 * A Class to represent a Placemark, including the id, letter the latitude and longitude and an
 * indication, whether it was visited or not.
 */

public class PlaceMark {

    public String id;
    public String letter;
    public double latitude;
    public double longitude;
    private boolean visited;

    public PlaceMark() {
        this.visited = false;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
