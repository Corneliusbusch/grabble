package selp.grabble;


import java.util.Date;

/**
 * Created by nicole on 06.11.2016.
 *
 * Class to represent a word, including the value and the date of creation
 */

public class WordInfo {

    public String word;
    public int maxPoints;
    public Date firstDate;

    public WordInfo(String word, int points, Date date) {
        this.word = word;
        maxPoints = points;
        firstDate = date;
    }

    public String toValueString() {
        return word + ", " + maxPoints + ", " + firstDate;
    }

}
